import { Component, OnInit } from '@angular/core';

import { Message } from '../message';
import { MessagesService } from '../messages.service';

@Component({
  selector: 'app-guestbook',
  templateUrl: './guestbook.component.html',
  styleUrls: ['./guestbook.component.css']
})
export class GuestbookComponent implements OnInit {
  messages: Message[] = [];

  currentAuthor = 'Tom';
  currentMessage = '';

  constructor(private readonly messagesService: MessagesService) { }

  ngOnInit(): void {
    this.updateMessages();
  }

  updateMessages() {
    this.messagesService.getMessages().subscribe(messages => {
      this.messages = messages;
    });
  }

  sendMessage() {
    if (this.currentAuthor !== '' && this.currentMessage !== '') {
      this.messagesService.sendMessage({
        author: this.currentAuthor,
        message: this.currentMessage
      }).subscribe(message => {
        // POST request completed, updating messages
        this.updateMessages();
      });
      this.currentMessage = '';
    }
  }

}
